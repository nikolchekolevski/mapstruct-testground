package mapper;

import lombok.extern.java.Log;
import mapstruct.dto.CarDto;
import mapstruct.dto.OwnerTypeDto;
import mapstruct.mapper.CarMapper;
import mapstruct.mapper.OwnerTypeMapper;
import mapstruct.model.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Log
public class MappstructCarMapperTest {

    CarDto carDto;
    CarDto carDto2;
    CarDto carDto3;
    List<CarDto> carDtos = new ArrayList<CarDto>();
    OwnerTypeDto ownerTypeDto;

    @Before
    public void createCustomer() {
        carDto = CarDto.builder().color("green").vin("VIN").wheels(4).manufacturerCountry("DE").manufacturerName("VW").doors(5).seats(5).manufactureDate("11.11.2011").owner("Smith").ownerType(OwnerTypeDto.ORGANIZATION).build();
        carDto2 = new CarDto("VIN", 4, "VW", "DE", 5, 5, "green", "11.11.2011", OwnerTypeDto.ORGANIZATION, "Smith");
        carDto3 = new CarDto("VIN", 4, "FORD", "USA", 5, 5, "green", "12.11.2011", OwnerTypeDto.ORGANIZATION, "John");
        carDtos.add(carDto);
        carDtos.add(carDto2);
        carDtos.add(carDto3);
        ownerTypeDto = OwnerTypeDto.ORGANIZATION;
    }

    @Test
    public void mapCarDtoToCar() {
        Car car = CarMapper.INSTANCE.mapFromDto(carDto);
        assertCarToCarDto(car, carDto);
    }

    @Test
    public void mapCarDtosToCars() {
        List<Car> cars = CarMapper.INSTANCE.mapListToCar(carDtos);
        for (int i = 0; i < cars.size(); i++) {
            assertCarToCarDto(cars.get(i), carDtos.get(i));
        }
    }

    @Test
    public void mapCarToCarDto() {
        String startDateString = "12/11/2011";
        DateFormat df = new SimpleDateFormat("mm/dd/yyyy");
        Date startDate = null;
        try {
            startDate = df.parse(startDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Car car = Car.builder().carInformation(CarInformation.builder().carDetails(new CarDetails("green", 2, 2)).manufactureDate(startDate).build()).manufacturer(new Manufacturer("Ford", "USA")).owner(Owner.builder().ownerType(OwnerType.COMPANY).name("John").build()).vin("VIN").wheels(4).build();

        CarDto dto = CarMapper.INSTANCE.mapToDto(car);
        assertCarToCarDto(car, dto);
    }

    public void assertCarToCarDto(Car car, CarDto carDto) {
        Assert.assertEquals(car.getVin(), carDto.getVin());
        Assert.assertEquals(car.getWheels(), carDto.getWheels());

        Assert.assertEquals(car.getManufacturer().getName(), carDto.getManufacturerName());
        Assert.assertEquals(car.getManufacturer().getCountry(), carDto.getManufacturerCountry());

        DateFormat df = new SimpleDateFormat("dd.mm.yyyy");
        try {
            Assert.assertEquals(car.getCarInformation().getManufactureDate(), df.parse(carDto.getManufactureDate()));
        } catch (ParseException e) {
            log.severe(e.getMessage());
        }

        Assert.assertEquals(car.getCarInformation().getCarDetails().getColor(), carDto.getColor());
        Assert.assertEquals(car.getCarInformation().getCarDetails().getSeats(), carDto.getSeats());
        Assert.assertEquals(car.getCarInformation().getCarDetails().getDoors(), carDto.getDoors());

        Assert.assertEquals(car.getOwner().getName(), carDto.getOwner());
        Assert.assertEquals(car.getOwner().getOwnerType(), OwnerType.COMPANY);

        if (car.getOwner().getFullOwner() != null)
            Assert.assertEquals(car.getOwner().getFullOwner(), carDto.getOwner() + " " + carDto.getOwnerType());
    }

    @Test
    public void mapOwnerTypeDtoToOwnerType() {
        OwnerType ownerType = OwnerTypeMapper.INSTANCE.mapFromDto(ownerTypeDto);

        Assert.assertTrue(ownerType.getShortName().equals("company"));
        Assert.assertTrue(ownerType.equals(OwnerType.COMPANY));
    }

    @Test
    public void carMapperDefaultAndConstant() {
        carDto.setVin(null);
        carDto.setWheels(2);
        Car car = CarMapper.INSTANCE.mapFromDto(carDto);

        Assert.assertNotEquals(car.getVin(), carDto.getVin());
        Assert.assertEquals(car.getVin(), "DEFAULT_VIN");

        Assert.assertNotEquals(car.getWheels(), carDto.getWheels());
        Assert.assertEquals(car.getWheels(), 4);
    }
}
