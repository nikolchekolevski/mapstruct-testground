package mapper;

import lombok.extern.java.Log;
import mapstruct.dto.CarDto;
import mapstruct.dto.OwnerTypeDto;
import mapstruct.model.Car;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

@Log
public class LombokTests {
    CarDto carDto;
    CarDto carDto2;
    CarDto carDto3;
    Car car;

    @Before
    public void createCustomer() {
        carDto = CarDto.builder().color("green").vin("VIN").wheels(4).manufacturerCountry("DE").manufacturerName("VW").doors(5).seats(5).manufactureDate("11.11.2011").owner("Smith").ownerType(OwnerTypeDto.ORGANIZATION).build();
        carDto2 = new CarDto("VIN", 4, "VW", "DE", 5, 5, "green", "12.11.1992", OwnerTypeDto.ORGANIZATION, "Smith");
        carDto3 = new CarDto("VIN", 4, "VW", "DE", 5, 5, "green", "11.11.2011", OwnerTypeDto.PERSON, "Smith");
    }

    @Test
    public void carEquals() {
        Assert.assertTrue(carDto.equals(carDto3));
        Assert.assertFalse(carDto.equals(carDto2));
        log.info("The two objects are equal.");
    }

    @Test(expected = NullPointerException.class)
    public void notNullExceptionTest() {
        CarDto notNullException = new CarDto("VIN", 4, "VW", "DE", 5, 5, "green", "11.11.2011", OwnerTypeDto.ORGANIZATION, null);
    }

    @Test(expected = NullPointerException.class)
    public void notNullExceptionSetterTest() {
        CarDto notNullException = new CarDto();
        notNullException.setOwner(null);
    }

    @Test
    public void carDtoToStringTest() {
        Assert.assertEquals(carDto.toString(),"CarDto(vin=VIN, wheels=4, manufacturerName=VW, manufacturerCountry=DE, manufactureDate=11.11.2011)");
    }
}
