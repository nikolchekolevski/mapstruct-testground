package mapstruct.mapper;

import mapstruct.dto.CarDto;
import mapstruct.model.Car;

public abstract class OwnerDecorator implements CarMapper{
    private final CarMapper delegate;

    public OwnerDecorator(CarMapper delegate) {
        this.delegate = delegate;
    }

    @Override
    public Car mapFromDto(CarDto dto) {
        Car car = delegate.mapFromDto( dto );
        car.getOwner().setFullOwner( dto.getOwner() + " " + dto.getOwnerType() );
        return car;
    }
}
