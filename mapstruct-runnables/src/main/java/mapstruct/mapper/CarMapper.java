package mapstruct.mapper;

import mapstruct.dto.CarDto;
import mapstruct.model.Car;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses=OwnerTypeMapper.class)
@DecoratedWith(OwnerDecorator.class)
public interface CarMapper {

    public CarMapper INSTANCE = Mappers.getMapper(CarMapper.class);

    @Mappings({
            @Mapping(source = "vin", target = "vin",defaultValue = "DEFAULT_VIN"),
            @Mapping(target = "wheels", constant = "4"),
            @Mapping(source = "color", target = "carInformation.carDetails.color"),
            @Mapping(source = "seats", target = "carInformation.carDetails.seats"),
            @Mapping(source = "doors", target = "carInformation.carDetails.doors"),
            @Mapping(source = "manufactureDate", target = "carInformation.manufactureDate", dateFormat = "dd.mm.yyyy"),
            @Mapping(source = "manufacturerName", target = "manufacturer.name"),
            @Mapping(source = "manufacturerCountry", target = "manufacturer.country"),
            @Mapping(source = "owner", target = "owner.name"),
            @Mapping(source = "ownerType", target = "owner.ownerType"),
    })
    Car mapFromDto(CarDto carDto);

    @InheritInverseConfiguration(name = "mapFromDto")
    CarDto mapToDto(Car customer);

    List<Car> mapListToCar(List<CarDto> carDtos);

}
