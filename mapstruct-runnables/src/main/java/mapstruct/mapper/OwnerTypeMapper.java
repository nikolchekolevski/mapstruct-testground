package mapstruct.mapper;

import mapstruct.dto.OwnerTypeDto;
import mapstruct.model.OwnerType;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.ValueMapping;
import org.mapstruct.ValueMappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface OwnerTypeMapper {

    public OwnerTypeMapper INSTANCE = Mappers.getMapper(OwnerTypeMapper.class);

    @ValueMappings({
            @ValueMapping(source = "ORGANIZATION", target = "COMPANY"),
            @ValueMapping(source = "PERSON", target = "PERSON"),
    })
    OwnerType mapFromDto(OwnerTypeDto ownerTypeDto);

    @InheritInverseConfiguration
    OwnerTypeDto mapToDto(OwnerType ownerType);

    List<OwnerTypeDto> mapListToDto(List<OwnerType> ownerTypes);
}
