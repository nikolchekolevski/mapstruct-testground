package mapstruct.mapper;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import mapstruct.dto.OwnerTypeDto;
import mapstruct.model.OwnerType;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2016-10-17T11:12:23+0200",
    comments = "version: 1.1.0.CR1, compiler: javac, environment: Java 1.8.0_73 (Oracle Corporation)"
)
public class OwnerTypeMapperImpl implements OwnerTypeMapper {

    @Override
    public OwnerType mapFromDto(OwnerTypeDto ownerTypeDto) {
        if ( ownerTypeDto == null ) {
            return null;
        }

        OwnerType ownerType;

        switch ( ownerTypeDto ) {
            case ORGANIZATION: ownerType = OwnerType.COMPANY;
            break;
            case PERSON: ownerType = OwnerType.PERSON;
            break;
            default: throw new IllegalArgumentException( "Unexpected enum constant: " + ownerTypeDto );
        }

        return ownerType;
    }
    @Override
    public OwnerTypeDto mapToDto(OwnerType ownerType) {
        if ( ownerType == null ) {
            return null;
        }

        OwnerTypeDto ownerTypeDto;

        switch ( ownerType ) {
            case COMPANY: ownerTypeDto = OwnerTypeDto.ORGANIZATION;
            break;
            case PERSON: ownerTypeDto = OwnerTypeDto.PERSON;
            break;
            default: throw new IllegalArgumentException( "Unexpected enum constant: " + ownerType );
        }

        return ownerTypeDto;
    }
    @Override
    public List<OwnerTypeDto> mapListToDto(List<OwnerType> ownerTypes) {
        if ( ownerTypes == null ) {
            return null;
        }

        List<OwnerTypeDto> list = new ArrayList<OwnerTypeDto>();
        for ( OwnerType ownerType : ownerTypes ) {
            list.add( mapToDto( ownerType ) );
        }

        return list;
    }
}
