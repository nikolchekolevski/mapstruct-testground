package mapstruct.mapper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.Generated;
import mapstruct.dto.CarDto;
import mapstruct.model.Car;
import mapstruct.model.CarDetails;
import mapstruct.model.CarInformation;
import mapstruct.model.Manufacturer;
import mapstruct.model.Owner;
import mapstruct.model.OwnerType;
import org.mapstruct.factory.Mappers;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2016-10-17T11:18:45+0200",
    comments = "version: 1.1.0.CR1, compiler: javac, environment: Java 1.8.0_73 (Oracle Corporation)"
)
public class CarMapperImpl implements CarMapper {

    private final OwnerTypeMapper ownerTypeMapper = Mappers.getMapper( OwnerTypeMapper.class );

    @Override
    public Car mapFromDto(CarDto carDto) {
        if ( carDto == null ) {
            return null;
        }

        Car car = new Car();

        Owner owner = new Owner();
        Manufacturer manufacturer = new Manufacturer();
        CarDetails carDetails = new CarDetails();
        CarInformation carInformation = new CarInformation();
        car.setManufacturer( manufacturer );
        car.setCarInformation( carInformation );
        carInformation.setCarDetails( carDetails );
        car.setOwner( owner );

        carDetails.setDoors( carDto.getDoors() );
        if ( carDto.getManufactureDate() != null ) {
            try {
                carInformation.setManufactureDate( new SimpleDateFormat( "dd.mm.yyyy" ).parse( carDto.getManufactureDate() ) );
            }
            catch ( ParseException e ) {
                throw new RuntimeException( e );
            }
        }
        owner.setName( carDto.getOwner() );
        owner.setOwnerType( ownerTypeMapper.mapFromDto( carDto.getOwnerType() ) );
        carDetails.setColor( carDto.getColor() );
        manufacturer.setName( carDto.getManufacturerName() );
        carDetails.setSeats( carDto.getSeats() );
        manufacturer.setCountry( carDto.getManufacturerCountry() );

        return car;
    }

    @Override
    public CarDto mapToDto(Car customer) {
        if ( customer == null ) {
            return null;
        }

        CarDto carDto = new CarDto();

        carDto.setDoors( customerCarInformationCarDetailsDoors( customer ) );
        carDto.setOwner( customerOwnerName( customer ) );
        carDto.setOwnerType( ownerTypeMapper.mapToDto( customerOwnerOwnerType( customer ) ) );
        carDto.setColor( customerCarInformationCarDetailsColor( customer ) );
        carDto.setManufacturerName( customerManufacturerName( customer ) );
        carDto.setManufactureDate( new SimpleDateFormat( "dd.mm.yyyy" ).format( customerCarInformationManufactureDate( customer ) ) );
        carDto.setSeats( customerCarInformationCarDetailsSeats( customer ) );
        carDto.setManufacturerCountry( customerManufacturerCountry( customer ) );

        return carDto;
    }

    @Override
    public List<CarDto> mapListToDto(List<Car> customers) {
        if ( customers == null ) {
            return null;
        }

        List<CarDto> list = new ArrayList<CarDto>();
        for ( Car car : customers ) {
            list.add( mapToDto( car ) );
        }

        return list;
    }

    private int customerCarInformationCarDetailsDoors(Car car) {

        if ( car == null ) {
            return 0;
        }
        CarInformation carInformation = car.getCarInformation();
        if ( carInformation == null ) {
            return 0;
        }
        CarDetails carDetails = carInformation.getCarDetails();
        if ( carDetails == null ) {
            return 0;
        }
        int doors = carDetails.getDoors();
        return doors;
    }

    private String customerOwnerName(Car car) {

        if ( car == null ) {
            return null;
        }
        Owner owner = car.getOwner();
        if ( owner == null ) {
            return null;
        }
        String name = owner.getName();
        if ( name == null ) {
            return null;
        }
        return name;
    }

    private OwnerType customerOwnerOwnerType(Car car) {

        if ( car == null ) {
            return null;
        }
        Owner owner = car.getOwner();
        if ( owner == null ) {
            return null;
        }
        OwnerType ownerType = owner.getOwnerType();
        if ( ownerType == null ) {
            return null;
        }
        return ownerType;
    }

    private String customerCarInformationCarDetailsColor(Car car) {

        if ( car == null ) {
            return null;
        }
        CarInformation carInformation = car.getCarInformation();
        if ( carInformation == null ) {
            return null;
        }
        CarDetails carDetails = carInformation.getCarDetails();
        if ( carDetails == null ) {
            return null;
        }
        String color = carDetails.getColor();
        if ( color == null ) {
            return null;
        }
        return color;
    }

    private String customerManufacturerName(Car car) {

        if ( car == null ) {
            return null;
        }
        Manufacturer manufacturer = car.getManufacturer();
        if ( manufacturer == null ) {
            return null;
        }
        String name = manufacturer.getName();
        if ( name == null ) {
            return null;
        }
        return name;
    }

    private Date customerCarInformationManufactureDate(Car car) {

        if ( car == null ) {
            return null;
        }
        CarInformation carInformation = car.getCarInformation();
        if ( carInformation == null ) {
            return null;
        }
        Date manufactureDate = carInformation.getManufactureDate();
        if ( manufactureDate == null ) {
            return null;
        }
        return manufactureDate;
    }

    private int customerCarInformationCarDetailsSeats(Car car) {

        if ( car == null ) {
            return 0;
        }
        CarInformation carInformation = car.getCarInformation();
        if ( carInformation == null ) {
            return 0;
        }
        CarDetails carDetails = carInformation.getCarDetails();
        if ( carDetails == null ) {
            return 0;
        }
        int seats = carDetails.getSeats();
        return seats;
    }

    private String customerManufacturerCountry(Car car) {

        if ( car == null ) {
            return null;
        }
        Manufacturer manufacturer = car.getManufacturer();
        if ( manufacturer == null ) {
            return null;
        }
        String country = manufacturer.getCountry();
        if ( country == null ) {
            return null;
        }
        return country;
    }
}
