package mapstruct.dto;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode(of={"manufacturerName","manufacturerCountry","manufactureDate"})
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude={"ownerType","owner","doors","seats","color"})
@Builder
public class CarDto {
    private String vin;
    private int wheels;
    private String manufacturerName;
    private String manufacturerCountry;
    private int seats;
    private int doors;
    private String color;
    private String manufactureDate;
    private OwnerTypeDto ownerType;
    @NonNull
    private String owner;
}
