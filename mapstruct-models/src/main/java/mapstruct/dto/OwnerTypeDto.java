package mapstruct.dto;

import lombok.Getter;
import lombok.Setter;

public enum OwnerTypeDto {
    ORGANIZATION("organization"),
    PERSON("person");

    @Getter
    @Setter
    private String shortName;
    OwnerTypeDto(String shortName){
        setShortName(shortName);
    }
}
