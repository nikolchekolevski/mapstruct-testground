package mapstruct.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
public class Manufacturer {
    private String name;
    private String country;
}
