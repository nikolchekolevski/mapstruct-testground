package mapstruct.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
public class Car {
    private String vin;
    private int wheels;
    private Manufacturer manufacturer;
    private CarInformation carInformation;
    private Owner owner;
}
