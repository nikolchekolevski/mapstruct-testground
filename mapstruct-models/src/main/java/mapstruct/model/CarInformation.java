package mapstruct.model;

import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
public class CarInformation {
    private CarDetails carDetails;
    private Date manufactureDate;
}
