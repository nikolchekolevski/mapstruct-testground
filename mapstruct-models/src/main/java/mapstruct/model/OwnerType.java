package mapstruct.model;

import lombok.Getter;
import lombok.Setter;

public enum OwnerType {
        COMPANY("company"),
        PERSON("person");

    @Getter
    @Setter
    private String shortName;
    OwnerType(String shortName){
        setShortName(shortName);
    }
}
