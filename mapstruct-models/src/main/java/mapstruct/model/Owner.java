package mapstruct.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
public class Owner {
    private OwnerType ownerType;
    private String name;
    private String fullOwner;
}
