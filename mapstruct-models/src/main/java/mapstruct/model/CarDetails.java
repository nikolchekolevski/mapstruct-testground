package mapstruct.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CarDetails {
    private String color;
    private int doors;
    private int seats;
}
